### Building from sources
- Install sphinx
- `pip install -r requirements.txt`
- `make html`

### Translate

We follow the ReadTheDocs workflow: https://docs.readthedocs.io/en/stable/guides/manage-translations-sphinx.html#translate-text-from-source-language

Install `pip install sphinx-intl`, you may need to run it as `sudo`.

#### Update translation files

- Update source files: `sphinx-build -b gettext . _build/gettext`
- Update translation files: `sphinx-intl update -p _build/gettext -l $lang`
    Replace `$lang` by the tranlation language or set is as a local environment variable: `export lang=en` / `set lang=en`.
- Build the translated files: `sphinx-build -b html -D language=$lang . _build/html/$lang`

#### Translate hyperlinks
When a word is supposed to be a hyperlink and indicated with the link directive `word_`, you must update the reference with the link in the `.po` file.

For example if the word is `ici_` and has a link to `.. _ici: http://my_website.info`, the translation must be `here_` with `.. here_: ici_`.