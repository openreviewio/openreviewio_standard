.. OpenReviewIO documentation master file, created by
   sphinx-quickstart on Tue Apr 28 11:14:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation du standard OpenReviewIO !
===============================================================

.. warning::

  OpenReviewIO est actuellement en phase **alpha** ! Il est suscepible de changer durant les prochains mois. Toute contribution est la bienvenue.

OpenReviewIO (ORIO) est un format d’échange d’informations de *review*.
Il permet de garantir leur compatibilité entre les outils.

Le format est basé sur le XML_.

.. _XML: https://fr.wikipedia.org/wiki/Extensible_Markup_Language

La description de ses classes est écrite en TOML_.

.. _TOML: https://github.com/toml-lang/toml

.. toctree::
   :maxdepth: 4

   Standard_Definition
   Version_1-0
   Integrations

.. important::

  Dernière version en date : :doc:`Version_1-0`.

  Les objets sont téléchargeables ici_.

Remerciements
-------------

Mille mercis à Jean-François Sarazin et Élie Michel pour leur aide et leur relecture.

Idée originale par Jean-François Sarazin.
Conception Félix David et Jean-François Sarazin.
Copyright 2020, Félix David ©

.. _ici: orio_classes_
.. _orio_classes: https://gitlab.com/openreviewio/openreviewio_standard/-/tree/master/orio_classes
