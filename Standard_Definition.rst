Versions
========
Dans l'optique de garantir une interopérabilité maximale entre les outils de *review* qui implémenteront ORIO, la compatibilité est déterminée par un numéro de version.

De cette façon, tous les outils de *review* affichant une compatibilité avec une même version pourront échanger sans difficulté leurs *reviews*.

La numérotation des versions utilise SemVer_.

.. _SemVer: https://semver.org/


Description du standard
========================================

.. toctree::
   :maxdepth: 2

OpenReviewIO permet de lier des informations dites de *review* à un média.

.. note:: ORIO a été conçu à l'origine pour la *review* d'animation.

Une *review* contient des notes, une note est composée de *contents*.

.. mermaid::

  graph RL
    media_path[media] -.- media
    subgraph Review
      media_path
      Status{{"Status"}} --> media_path
      note1 --> media_path
      note2 --> media_path

      attachment11 --> note1>Note 1]
      attachment12 --> note1>Note 1]
      attachment21 --> note2>Note 2]
      attachment22 --> note2>Note 2]

      subgraph contents
          attachment11("Text comment")
          attachment12("Image annotation")
          attachment21("Image annotation")
          attachment22("Image annotation")
      end
    end

Le format OpenReviewIO se présente sous forme d'un dossier dont le nom termine par ``.orio``. Il contient un fichier ``review.orio`` dans lequel toutes les informations de la *review* sont écrites et de dossiers où tous les fichiers utilisés comme contenu sont stockés. Les dossiers sont nommés comme la date de la note qu'ils concernent :

::

  review_name.orio
  │   review.orio
  └─── {note.date}
  │   │   image.png
  │   │   other_img.jpg


Pour stocker les informations dans le fichier ``review.orio``, le standard XML_ est choisi car très répandu et facilement lisible.

.. note::
  Les *reviewers* étant rarement très nombreux à travailler en même temps sur un même média, les concurrences en écriture du fichier XML_ sont négligeables.

Formats de fichiers
--------------------
Pour garantir une compatibilité maximale, il est nécessaire de limiter les possibilités accordées aux utilisateurs du format ORIO.

Alors qu'il est possible de lier des fichiers à une note, tous les outils de *review* ne supportent pas tous les types de fichiers. Il est donc nécessaire de spécifier les types acceptés dans les descriptions des objets à chaque nouvelle version d'ORIO.


Review
-------
Une *review* est liée à un media.

Tous les chemins contenus dans ``review.orio`` doivent être relatifs si les fichiers correspondants sont placés dans le dossier ``.orio`` dans des sous-dossiers au nom de la date de la note concernée.

.. note::

  Il est fortement conseillé de nommer le dossier de *review* du nom du média, extension comprise : **media_name.ext.orio**.
  Bien que le choix du nom soit libre.

Statut
--------
Le statut est lié à la *review*. Il permet de connaitre l'opération suivante à effectuer sur le media.

ORIO définit un nombre limité de statuts. Les statuts autorisés sont décrits dans chaque version.

OpenReviewIO garde un historique des changements de statuts. Le dernier statut en date étant le statut actuel de la *review*.


Note
----
Une note ressemble conceptuellement à un email, elle a une date, un auteur et un contenu qui peut prendre plusieurs formes (texte, image...).
Des notes se succèdent dans un fil thématique (la *review*) et une note peut répondre à une note précédemment créée.

Une note est par essence vide, les informations qu'elle contient sont des *contents*.

.. warning::
  Dans le cas d’ORIO, il est possible de modifier le contenu d’une note, mais cela est fortement déconseillé pour des raisons pratiques de suivi de production.

Contenu
----------
Les *contents* sont les données de la note.

Dans la conception, les *contents* suivent la philosophie de la programmation orientée objet, ils héritent d'autres *contents*. Un *content* hérite tous les paramètres de son parent et doit donc les renseigner.

La classe de référence est la classe ``Content``. Cette classe est de type ``Abstract``, ce qui empêche de l'utiliser telle quelle dans une note.

La définition des *contents* est stockée sous forme de fichier TOML_ suivant cette nomenclature : ``NomAttachment.toml``.

.. note::

  Pour une meilleure compréhension, une convention de nommage est établie pour le nom des *contents* :

  - ``{Name}Comment`` : Un commentaire concerne tout le media.
  - ``{Name}Annotation`` : Une annotation est relative à un fragment du média.


Classes
---------
OpenReviewIO décrit également des classes qui lui sont propres. Leurs paramètres sont écrits au format TOML_ :

-   Le nom d'une classe suit la convention CamelCase.

-   Les membres principaux de la classe sont à la racine du document.
-   Les types des membres sont écrits : ``membre = 'Type'``.

    -   Dans le cas d'un tableau : ``membre = 'Array[Type]'``.

-   Les paramètres à utiliser dans les APIs sont dans la table ``[parameters]``.

    - Les paramètres optionnels sont indiqués dans une table imbriquée ``[parameters.optional]``
    - Les valeurs par défaut des paramètres optionnels sont écrits dans un ``array`` en seconde position : ``membre = [ 'Type', '',]``.


.. important::

  Les objets sont téléchargeables ici_.

.. _ici: orio_classes_


.. _orio_classes: https://gitlab.com/openreviewio/openreviewio_standard/-/tree/master/orio_classes
.. _XML: https://fr.wikipedia.org/wiki/Extensible_Markup_Language
.. _TOML: https://github.com/toml-lang/toml
.. _rfc3339: https://tools.ietf.org/html/rfc3339
