###########
Version 1.0
###########

.. warning::

  Encore en **alpha** !

********
Classes
********

Status
======
**Classe abstraite**

Statuts possibles : ``approved``, ``rejected``, ``waiting review``. Le statut par défaut est ``waiting review``.

-   type : ``String`` = Abstract

-   parameters:

    -   date: ``String`` -> *Date de création en ISO UTC :* rfc3339_

    -   author: ``String``-> *Auteur de la note.*

    -   value: ``String``-> *État du statut.*

**XML**

.. code-block:: xml

  <status date="2020-10-13T10:38:14.173325+00:00" author="Alice">approved</status>

**Description TOML**

Contenu de ``Status.toml`` :

.. include:: ./orio_classes/Status.toml
  :literal:

Review
======
**Classe abstraite**

-   type : ``String`` = Abstract

-   parameters:

    -   media_path: ``String`` -> *Chemin vers le média. Les séquences d'image sont décrites avec un '#' par digit (image_seq-###.exr).*

    -   media_signature: ``String`` -> *Signature MD5 du media pour garantir son authenticité.*

    -   status_history: ``Dict{Dict}`` -> *Tous les changements statuts, le dernier de la liste étant le statut courant.*

    -   notes: ``Dict{Dict}`` -> *Notes associées à la review. La date de création est utilisée pour l'identification.*

    -   optional:

      -   metadata: *(optionel)* ``Dict{AnyType}`` = {} -> *Permet de stocker toute donnée supplémentaire relative à la review.*

**XML**

.. code-block:: xml

  <review media_path="/path/to/other_media.ext" media_signature="d41d8cd98f00b204e9800998ecf8427e">
    <metadata key="value"/>
    <statuses>
      ...
    </statuses>
    <notes>
      ...
    </notes>
  </review>

**Description TOML**

Contenu de ``Review.toml`` :

.. include:: ./orio_classes/Review.toml
  :literal:

Note
====
**Classe abstraite**

-   type : ``String`` = Abstract

-   parameters:

    -   date: ``String`` -> *Date de création en ISO UTC :* rfc3339_

    -   author: ``String`` -> *Auteur de la note.*

    -   optional:

      -   parent: *(optionel)* ``String`` = '' -> *Date (id) de la note à laquelle cette note est une réponse.*

      -   metadata: *(optionel)* ``Dict{AnyType}`` = {} -> *Permet de stocker toute donnée supplémentaire relative à la review.*

**XML**

.. code-block:: xml

  <note date="2020-10-13T10:38:14.173325+00:00" author="Michel" parent="">
    <contents>
      ...
    </contents>
    <metadata key="value"/>
  </note>

**Description TOML**

Contenu de ``Note.toml`` :

.. include:: ./orio_classes/Note.toml
  :literal:


Contents
===========

TextComment
-----------
Commentaire texte concernant tout le media.

-   type : ``String`` = Content. -> *Type de la classe dont sont héritées les propriétés.*

-   parameters :

    -   body : ``String`` -> *Contenu textuel du commentaire.*

**XML**

.. code-block:: xml

  <TextComment body="Text content body"/>

**Description TOML**

Contenu de ``TextComment.toml`` :

.. include:: ./orio_classes/TextComment.toml
  :literal:

TextAnnotation
--------------
Commentaire texte relatif à un fragment temporel d'un média. Composé d'une image de départ et d'une durée.

-   type : ``String`` = TextComment_. -> *Type de la classe dont sont héritées les propriétés.*

-   parameters :

    -   frame : ``Integer`` = 0 -> *Image à laquelle l'annotation commence.*

    -   duration : ``Integer`` = 1 -> *Durée de l'annotation en nombre d'images.*

**XML**

.. code-block:: xml

  <TextAnnotation body="Text annotation body" duration="7" frame="10"/>

**Description TOML**

Contenu de ``TextAnnotation.toml`` :

.. include:: ./orio_classes/TextAnnotation.toml
  :literal:

Image
-----
**Classe abstraite**. Les fichiers image acceptés sont les fichiers aux extensions JPEG, JPG et PNG.

-   type : ``String`` = Abstract
-   mime : ``Array[String]`` -> *MIME_ autorisé pour les images : JPG, JPEG or PNG.*

- parameters:

  -   path_to_image : ``String`` -> *Chemin vers le fichier image.*


.. _MIME: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types


**XML**

.. code-block:: xml

  <Image path_to_image="/path/to/image.png"/>

**Description TOML**

Contenu de ``Image.toml`` :

.. include:: ./orio_classes/Image.toml
  :literal:

ImageAnnotation
---------------
Annotation par image concernant une partie du media.

-   type: ``String`` = Content. -> *Type de la classe dont sont héritées les propriétés.*

-   paramètres :

    -   image : ``Image``
    -   frame : ``Integer`` = 0 -> *Image à laquelle l'annotation commence.*

    -   duration : ``Integer`` = 1 -> *Durée de l'annotation en nombre d'images.*

    -   optional:

      -   reference_image: *(optionel)* ``String`` = '' -> *Chemin vers le fichier image utilisé comme référence de l'annotation.*


**XML**

.. code-block:: xml

  <ImageAnnotation duration="10" frame="3" path_to_image="/path/to/image_annotation.png" reference_image="/path/to/reference_image.png"/>

**Description TOML**

Contenu de ``ImageAnnotation.toml`` :

.. include:: ./orio_classes/ImageAnnotation.toml
  :literal:


.. _rfc3339: https://tools.ietf.org/html/rfc3339


Fichier de démonstration
=========================

.. literalinclude:: ./review_1-0.orio
   :language: xml
